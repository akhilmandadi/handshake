const express = require('express');
const router = express.Router();
const logger = require('tracer').colorConsole();
const dotenv = require('dotenv');
dotenv.config();
var kafka = require('../kafka/client');
const { checkAuth } = require("../auth/auth");

router.get('/jobs', checkAuth, async (request, response) => {
  try {
    const data = {
      "body": request.body,
      "params": request.params,
      "query": request.query,
      "type": "getAllStudentJobs"
    }
    console.log(data)
    await kafka.make_request('student', data, function (err, data) {
      if (err) throw new Error(err)
      response.status(data.status).json(data.body);
    });
  } catch (ex) {
    logger.error(ex);
    const message = ex.message ? ex.message : 'Error while fetching jobs';
    const code = ex.statusCode ? ex.statusCode : 500;
    return response.status(code).json({ message });
  }
});

router.put('/student/:id/profile', checkAuth, async (request, response) => {
  try {
    const data = {
      "body": request.body,
      "params": request.params,
      "query": request.query,
      "type": "updateProfile"
    }
    await kafka.make_request('student', data, function (err, data) {
      if (err) throw new Error(err)
      response.status(data.status).json(data.body);
    });
  } catch (ex) {
    logger.error(ex);
    const message = ex.message ? ex.message : 'Error while Updating student profile';
    const code = ex.statusCode ? ex.statusCode : 500;
    return response.status(code).json({ message });
  }
});

router.post('/student/:id/profile', checkAuth, async (request, response) => {
    try {
      const data = {
        "body": request.body,
        "params": request.params,
        "query": request.query,
        "type": "postProfile"
      }
      await kafka.make_request('student', data, function (err, data) {
        if (err) throw new Error(err)
        response.status(data.status).json(data.body);
      });
  } catch (ex) {
    logger.error(ex);
    const message = ex.message ? ex.message : 'Error while Saving student education';
    const code = ex.statusCode ? ex.statusCode : 500;
    return response.status(code).json({ message });
  }
});

router.delete('/student/:id/profile', checkAuth, async (request, response) => {
  try {
    const data = {
      "body": request.body,
      "params": request.params,
      "query": request.query,
      "type": "deleteProfile"
    }
    await kafka.make_request('student', data, function (err, data) {
      if (err) throw new Error(err)
      response.status(data.status).json(data.body);
    });
  } catch (ex) {
    logger.error(ex);
    const message = ex.message ? ex.message : 'Error while deleting student education';
    const code = ex.statusCode ? ex.statusCode : 500;
    return response.status(code).json({ message });
  }
});

router.get('/student/:id/profile', checkAuth, async (request, response) => {
  try {
    const data = {
      "body": request.body,
      "params": request.params,
      "query": request.query,
      "type": "getProfile"
    }
    await kafka.make_request('student', data, function (err, data) {
      if (err) throw new Error(err)
      response.status(data.status).json(data.body);
    });
  } catch (ex) {
    logger.error(ex);
    const message = ex.message ? ex.message : 'Error while fetching student profile';
    const code = ex.statusCode ? ex.statusCode : 500;
    return response.status(code).json({ message });
  }
});

router.get('/student/profiles', checkAuth, async (request, response) => {
  try {
    const data = {
      "body": request.body,
      "params": request.params,
      "query": request.query,
      "type": "getStudentProfiles"
    }
    await kafka.make_request('student', data, function (err, data) {
      if (err) throw new Error(err)
      response.status(data.status).json(data.body);
    });
  } catch (ex) {
    logger.error(ex);
    const message = ex.message ? ex.message : 'Error while fetching profile details';
    const code = ex.statusCode ? ex.statusCode : 500;
    return response.status(code).json({ message });
  }
});

router.get('/events', checkAuth, async (request, response) => {
  try {
    const data = {
      "body": request.body,
      "params": request.params,
      "query": request.query,
      "type": "getEvents"
    }
    await kafka.make_request('student', data, function (err, data) {
      if (err) throw new Error(err)
      response.status(data.status).json(data.body);
    });
  } catch (ex) {
    logger.error(ex);
    const message = ex.message ? ex.message : 'Error while fetching events';
    const code = ex.statusCode ? ex.statusCode : 500;
    return response.status(code).json({ message });
  }
});

router.get('/student/:studentId/registrations', checkAuth, async (request, response) => {
  try {
    const data = {
      "body": request.body,
      "params": request.params,
      "query": request.query,
      "type": "getRegistrations"
    }
    await kafka.make_request('student', data, function (err, data) {
      if (err) throw new Error(err)
      response.status(data.status).json(data.body);
    });
  } catch (ex) {
    logger.error(ex);
    const message = ex.message ? ex.message : 'Error while fetching registrations details';
    const code = ex.statusCode ? ex.statusCode : 500;
    return response.status(code).json({ message });
  }
});

router.get('/student/:studentId/applications', checkAuth, async (request, response) => {
  try {
    const data = {
      "body": request.body,
      "params": request.params,
      "query": request.query,
      "type": "getStudentApplications"
    }
    await kafka.make_request('student', data, function (err, data) {
      if (err) throw new Error(err)
      response.status(data.status).json(data.body);
    });
  } catch (ex) {
    logger.error(ex);
    const message = ex.message ? ex.message : 'Error while fetching applications details';
    const code = ex.statusCode ? ex.statusCode : 500;
    return response.status(code).json({ message });
  }
});

module.exports = router;
