const express = require('express');
const router = express.Router();
const logger = require('tracer').colorConsole();
const _ = require('lodash');
const createError = require('http-errors');
const uuid = require('shortid');
const student = require('../db/schema/student').createModel();
const company = require('../db/schema/company').createModel();
const messages = require('../db/schema/message').createModel();
const operations = require('../db/operations');
const { auth } = require("../auth/auth");
const { checkAuth } = require("../auth/auth");
const uuidv1 = require('uuid/v1')
auth();
var kafka = require('../kafka/client');

router.get('/signin', async (request, response) => {
  try {
    const data = {
      "body": request.body,
      "params": request.params,
      "query": request.query,
      "type": "signin"
    }
    await kafka.make_request('common', data, function (err, data) {
      if (err) throw new Error(err)
      response.status(data.status).json(data.body);
    });
  } catch (ex) {
    logger.error(ex);
    const message = ex.message ? ex.message : 'Error while fetching credentials';
    const code = ex.statusCode ? ex.statusCode : 500;
    return response.status(code).json({ message });
  }
});

router.post('/signup', async (request, response) => {
  try {
    const data = {
      "body": request.body,
      "params": request.params,
      "query": request.query,
      "type": "signup"
    }
    await kafka.make_request('common', data, function (err, data) {
      if (err) throw new Error(err)
      response.status(data.status).json(data.body);
    });
  } catch (ex) {
    logger.error(ex);
    const message = ex.message ? ex.message : 'Error while fetching credentials';
    const code = ex.statusCode ? ex.statusCode : 500;
    return response.status(code).json({ message });
  }
});

router.get('/students', checkAuth, async (request, response) => {
  try {
    const data = {
      "body": request.body,
      "params": request.params,
      "query": request.query,
      "type": "getAllStudents"
    }
    await kafka.make_request('common', data, function (err, data) {
      if (err) throw new Error(err)
      response.status(data.status).json(data.body);
    });
  } catch (ex) {
    logger.error(ex);
    const message = ex.message ? ex.message : 'Error while fetching students details';
    const code = ex.statusCode ? ex.statusCode : 500;
    return response.status(code).json({ message });
  }
});

router.get('/messages/:id', checkAuth, async (request, response) => {
  try {
    const data = {
      "body": request.body,
      "params": request.params,
      "query": request.query,
      "type": "getMessages"
    }
    await kafka.make_request('conversations', data, function (err, data) {
      if (err) throw new Error(err)
      response.status(data.status).json(data.body);
    });
  } catch (ex) {
    logger.error(ex);
    const message = ex.message ? ex.message : 'Error while fetching messages';
    const code = ex.statusCode ? ex.statusCode : 500;
    return response.status(code).json({ message });
  }
});

router.post('/messages/:id', checkAuth, async (request, response) => {
  try {
    const data = {
      "body": request.body,
      "params": request.params,
      "query": request.query,
      "type": "postMessage"
    }
    await kafka.make_request('conversations', data, function (err, data) {
      if (err) throw new Error(err)
      response.status(data.status).json(data.body);
    });
  } catch (ex) {
    logger.error(ex);
    const message = ex.message ? ex.message : 'Error while saving messages';
    const code = ex.statusCode ? ex.statusCode : 500;
    return response.status(code).json({ message });
  }
});

module.exports = router;
