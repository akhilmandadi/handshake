const express = require('express');
const router = express.Router();
const logger = require('tracer').colorConsole();
const dotenv = require('dotenv');
dotenv.config();
const { checkAuth } = require("../auth/auth");
var kafka = require('../kafka/client');

router.get('/company/:id/jobs', checkAuth, async (request, response) => {
  try {
    const data = {
      "body": request.body,
      "params": request.params,
      "query": request.query,
      "type": "getAllJobs"
    }
    await kafka.make_request('company', data, function (err, data) {
      if (err) throw new Error(err)
      response.status(data.status).json(data.body);
    })
  } catch (ex) {
    logger.error(ex);
    const message = ex.message ? ex.message : 'Error while fetching jobs';
    const code = ex.statusCode ? ex.statusCode : 500;
    return response.status(code).json({ message });
  }
});

router.post('/company/:id/jobs', checkAuth, async (request, response) => {
  try {
    const data = {
      "body": request.body,
      "params": request.params,
      "query": request.query,
      "type": "createJob"
    }
    await kafka.make_request('company', data, function (err, data) {
      if (err) throw new Error(err)
      response.status(data.status).json(data.body);
    })
  } catch (ex) {
    logger.error(ex);
    const message = ex.message ? ex.message : 'Error while fetching jobs';
    const code = ex.statusCode ? ex.statusCode : 500;
    return response.status(code).json({ message });
  }
});

router.get('/company/:companyId/job/:jobId/applicants', checkAuth, async (request, response) => {
  try {
    const data = {
      "body": request.body,
      "params": request.params,
      "query": request.query,
      "type": "getApplicants"
    }
    await kafka.make_request('company', data, function (err, data) {
      if (err) throw new Error(err)
      response.status(data.status).json(data.body);
    })
  } catch (ex) {
    logger.error(ex);
    const message = ex.message ? ex.message : 'Error while fetching applicants';
    const code = ex.statusCode ? ex.statusCode : 500;
    return response.status(code).json({ message });
  }
});

router.put('/applications/:applicationId', checkAuth, async (request, response) => {
  try {
    const data = {
      "body": request.body,
      "params": request.params,
      "query": request.query,
      "type": "updateApplicationStatus"
    }
    await kafka.make_request('company', data, function (err, data) {
      if (err) throw new Error(err)
      response.status(data.status).json(data.body);
    })
  } catch (ex) {
    logger.error(ex);
    const message = ex.message ? ex.message : 'Error while Updating applicantion Status';
    const code = ex.statusCode ? ex.statusCode : 500;
    return response.status(code).json({ message });
  }
});

router.get('/job/:jobId', checkAuth, async (request, response) => {
  try {
    const data = {
      "body": request.body,
      "params": request.params,
      "query": request.query,
      "type": "getJobDetails"
    }
    await kafka.make_request('company', data, function (err, data) {
      if (err) throw new Error(err)
      response.status(data.status).json(data.body);
    })
  } catch (ex) {
    logger.error(ex);
    const message = ex.message ? ex.message : 'Error while fetching jobs details';
    const code = ex.statusCode ? ex.statusCode : 500;
    return response.status(code).json({ message });
  }
});

router.get('/company/:id/events', checkAuth, async (request, response) => {
  try {
    const data = {
      "body": request.body,
      "params": request.params,
      "query": request.query,
      "type": "getAllEvents"
    }
    await kafka.make_request('company', data, function (err, data) {
      if (err) throw new Error(err)
      response.status(data.status).json(data.body);
    })
  } catch (ex) {
    logger.error(ex);
    const message = ex.message ? ex.message : 'Error while fetching events';
    const code = ex.statusCode ? ex.statusCode : 500;
    return response.status(code).json({ message });
  }
});

router.post('/company/:id/events', checkAuth, async (request, response) => {
  try {
    const data = {
      "body": request.body,
      "params": request.params,
      "query": request.query,
      "type": "createEvent"
    }
    await kafka.make_request('company', data, function (err, data) {
      if (err) throw new Error(err)
      response.status(data.status).json(data.body);
    })
  } catch (ex) {
    logger.error(ex);
    const message = ex.message ? ex.message : 'Error while posting event';
    const code = ex.statusCode ? ex.statusCode : 500;
    return response.status(code).json({ message });
  }
});

router.get('/company/:companyId/event/:eventId/applicants', checkAuth, async (request, response) => {
  try {
    const data = {
      "body": request.body,
      "params": request.params,
      "query": request.query,
      "type": "getApplications"
    }
    await kafka.make_request('company', data, function (err, data) {
      if (err) throw new Error(err)
      response.status(data.status).json(data.body);
    })
  } catch (ex) {
    logger.error(ex);
    const message = ex.message ? ex.message : 'Error while fetching event applicants';
    const code = ex.statusCode ? ex.statusCode : 500;
    return response.status(code).json({ message });
  }
});

router.get('/company', checkAuth, checkAuth, async (request, response) => {
  try {
    const data = {
      "body": request.body,
      "params": request.params,
      "query": request.query,
      "type": "getCompanyProfile"
    }
    await kafka.make_request('company', data, function (err, data) {
      if (err) throw new Error(err)
      response.status(data.status).json(data.body);
    })
  } catch (ex) {
    logger.error(ex);
    const message = ex.message ? ex.message : 'Error while fetching company details';
    const code = ex.statusCode ? ex.statusCode : 500;
    return response.status(code).json({ message });
  }
});

router.put('/company/:id/profile', checkAuth, async (request, response) => {
  try {
    const data = {
      "body": request.body,
      "params": request.params,
      "query": request.query,
      "type": "updateCompanyProfile"
    }
    await kafka.make_request('company', data, function (err, data) {
      if (err) throw new Error(err)
      response.status(data.status).json(data.body);
    })
  } catch (ex) {
    logger.error(ex);
    const message = ex.message ? ex.message : 'Error while Updating company profile';
    const code = ex.statusCode ? ex.statusCode : 500;
    return response.status(code).json({ message });
  }
});

router.get('/event/:id', async (request, response) => {
  try {
    const data = {
      "body": request.body,
      "params": request.params,
      "query": request.query,
      "type": "getEventsDetails"
    }
    await kafka.make_request('company', data, function (err, data) {
      if (err) throw new Error(err)
      response.status(data.status).json(data.body);
    })
  } catch (ex) {
    logger.error(ex);
    const message = ex.message ? ex.message : 'Error while fetching event details';
    const code = ex.statusCode ? ex.statusCode : 500;
    return response.status(code).json({ message });
  }
});

router.post('/event/:eventId/register', checkAuth, async (request, response) => {
  try {
    const data = {
      "body": request.body,
      "params": request.params,
      "query": request.query,
      "type": "registerForEvent"
    }
    await kafka.make_request('company', data, function (err, data) {
      if (err) throw new Error(err)
      response.status(data.status).json(data.body);
    })
  } catch (ex) {
    logger.error(ex);
    const message = ex.message ? ex.message : 'Error while fetching event details';
    const code = ex.statusCode ? ex.statusCode : 500;
    return response.status(code).json({ message });
  }
});

module.exports = router;
